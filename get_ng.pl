#!/usr/bin/perl
use strict;

my $linenum = 0;
my $chapter;
my $elem;
my $A;
my $line;

open(REACLIB, "< reaclib");
while($line = <REACLIB>) {
  $linenum += 1;
  
  if ($line =~ /^(\d{1,2})$/) {
    $chapter = $1;
    next;
  }

  if ($chapter eq 4 && $line =~ /^\s{5}\s{4}n\s{0,4}([a-zA-Z]{1,3})(\d{1,3})/) {
    $elem = $1;
    $A = $2;
    printf "%s %d\n", $elem, $A; 
  }
}
close(REACLIB);
