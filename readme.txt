Usage
=====
1. Generate a list of (n,g) reactions from reaclib file:
   $ ./get_ng.pl > ng_reactions.txt

2. Specify TALYS binary location in 'runtalys.pl' '$talys' variable.

3. Run evaluation (use 'tmux' or 'nohup' to make it session independent):
   $ ./evaluate.pl | tee log.txt
   This will take time.
   Script creates 'talysdata' directory, where you can find lots of 
   subdirectories with results of TALYS evaluation of neutron-nuclei 
   reactions. The most interesting file in those subdirectories is
   'myreaclib.txt', that contains REACLIB fit for (n,g) reactions.
   
4. To show the last 10 evaluated nuclei execute
   $ ls talysdata/ -Art -l --time-style=local | tail -n 10
   This might be helpful, if the previous calculation was interupted, 
   so you'll need to know which directory contains broken data. 

