#!/usr/bin/perl
use Try::Tiny;

sub sig_int_handler() {
    warn "Exiting from SIGINT\n";
    exit(128 | 2);   # 2 = SIGINT
};

$SIG{INT} = \&sig_int_handler;

# Specify TALYS binary path
$talys = "~/talys-1.95-hack/talys";
# this is not binary itself, only its location!

# Get element and mass number
$elem = $ARGV[0];
$A = $ARGV[1];
$elem =~ /^[A-Za-z]+$/ or die "Bad element name '$elem'\n";
$elem = lc $elem;
$A =~ /^\d+$/ or die "Bad mass number '$A'\n";

# Create elements and charges dictationaries
my %names;
my %znums;
open(ELEMENTS, "< elements.txt") or die "Failed to read elements.txt: $!\n";
while(<ELEMENTS>) 
{
  if (/^\s*(\d+)\s+([A-Za-z]+)\s*$/) 
  {
    $name = lc $2;
    $z = $1;
    $znums{$name} = $z;
    $names{$z} = $name; 
  }
}
close(ELEMENTS);


# Create directory
if (not -e "talysdata") {
    mkdir "talysdata" or die "Failed to create 'talysdata': $!\n";
  }
open (my $REACLIB, "<", "reaclib");
$dirname = "talysdata/$elem-$A";
if (-e $dirname) {
  printf "Directory '$dirname' already exists";
  exit 0;
}
mkdir $dirname or die "Failed to create '$dirname': $!\n";
chdir $dirname or die "Failed to cd '$dirname': $!\n";


# Select disctable
# Use mode 3, as most of nuclei we study are exotic ones.
# BUT if there is stable one with nice experimental data,
# use experimental levels (mode 1) 
$disctable = 3;
open(LEVELS, "<", sprintf("$talys/structure/levels/final/%s.lev", ucfirst($elem)));
while(<LEVELS>) 
{
  @F = split;
  if (@F == 5 && $F[4] =~ /$A$elem/i && $F[3] > 5)
  {
    $disctable = 1;
    last;
  }
}
close(LEVELS);



# Print configuration
open(INPUT, ">", "$A-$elem-input") or die "Failed to create input file: $!\n";
print INPUT <<EOF;
projectile n
element $elem
mass $A
energy 1
astro y
disctable $disctable
EOF
close(INPUT);


# Run TALYS
printf "  Rates evaluation started\n";
system("$talys/talys < $A-$elem-input > $A-$elem-output");
if ($? & 127) {
  my $sig = $? & 127;
  die "Caught signal INT" if $sig == 2;  # die on ctrl-c 
}
printf "  Rates evaluation finished\n";
# Comment the second line of all output files
system(q!perl -i -pe 's/^/#/ if $. == 2' astrorate.tot!);



# Generate individual reaction files via GNU Octave
$z = $znums{$elem};
$nucl = "$elem$A";
$file_ng = "r_${nucl}_ng_" . $elem . ($A + 1);
$file_nn = "r_${nucl}_nn_" . $nucl;
$file_np = "r_${nucl}_np_" . $names{$z - 1} . $A;
$file_na = "r_${nucl}_na_" . $names{$z - 2} . ($A - 3);

while (<DATA>) {    
  $mkrates .= $_;
}

$mkrates =~ s/FILENG/$file_ng/g;
$mkrates =~ s/FILENN/$file_nn/g;
$mkrates =~ s/FILENP/$file_np/g;
$mkrates =~ s/FILENA/$file_na/g;

open(MKRATES, "> mkrates.m") or die "can't open mkrates.m: $!\n";
print MKRATES $mkrates;
close(MKRATES);

system("octave -q mkrates.m");

__DATA__
astrorates = load('astrorate.tot');
t9 = astrorates(:,1);
ng = astrorates(:,3);
nn = astrorates(:,4);
np = astrorates(:,5);
na = astrorates(:,6);

format long E

fng = fopen('FILENG', 'w');
fdisp(fng, [t9, ng]);
fclose(fng);

fnn = fopen('FILENN', 'w');
fdisp(fnn, [t9, nn]);
fclose(fnn);

fnp = fopen('FILENP', 'w');
fdisp(fnp, [t9, np]);
fclose(fnp);

fna = fopen('FILENA', 'w');
format long E
fdisp(fna, [t9, na]);
fclose(fna);

