#!/usr/bin/perl
use strict;
use POSIX qw(strftime);

open (my $NGREACTIONS, "<", "ng_reactions.txt");
my @lines = <$NGREACTIONS>;
close $NGREACTIONS;
my $reacnum = scalar @lines;
my $curnum = 0;
my $existed = 0;
my $smallA = 0;

#sub sig_int_handler() {
##   die "Dying from SIGINT\n";
#    warn "Exiting from SIGINT\n";
#    exit(128 | 2);   # 2 = SIGINT
#};
#
#$SIG{INT} = \&sig_int_handler;



for my $line (@lines) {
  $curnum++;
  $line =~ /^([a-zA-Z]+)\s(\d+)/ or die "Wrong line format: $line\n";
  my $elem = $1;
  my $A = $2;
  
  my $time = strftime "%F %T", localtime;
  printf "$time - $curnum/$reacnum: $elem$A(n,g)$elem" . ($A + 1) . "\n"; 

  if (($A < 40) or ($elem eq "n") or ($elem eq "p")) {
    printf "'$elem $A' has too small A\n";
    $smallA++;
    next;
  }
  
  my $dirname = "talysdata/$elem-$A";
  if (-e $dirname) {
    printf "Directory '$dirname' already exists\n";
    $existed++;
    next;
  }

  system("./runtalys.pl", $elem, $A);
  my $dir = "talysdata/${elem}-${A}";
  my $ratefile = "${dir}/r_${elem}${A}_ng_${elem}" . ($A + 1);
  my $destfile = "${dir}/reaclib";
  system("./r2reaclib.pl", $ratefile);
}

printf "Already existed: $existed\n";
printf "Small A: $smallA\n";

