#!/usr/bin/perl

use strict;

# e.g.: r_ag100_ng_ag101
my $filename = $ARGV[0];
$filename =~ /^(.*)\/r_(.*)$/;
my $dir = $1;
my $reac = $2;
$reac =~ /^([a-zA-Z]+\d+)_([a-z]+)([a-z]+)_([a-zA-Z]+\d+)$/;
my $inucl = lc $1;
my $ipart = $2;  # !!! assuming it is always 'n' !!!
my $fpart = $3;
my $fnucl = lc $4;

my $l1 = sprintf "%5s%5s%5s", "", $ipart, $inucl;
if (not $fpart eq "g") {
  $l1 .= sprintf "%5s", $fpart;
}
$l1 .= sprintf "%5s%10s%8s%4s%1s%1s%3s%12.5E%10s",
        $fnucl, "", "", "ngha", "", "", "", 0, "";

open (my $DESTFILE, "> $dir/myreaclib.txt");
#print $DESTFILE "4\n";  # !!! assuming it is (n,g); otherwise it is another chapter !!!
print $DESTFILE "$l1\n";
close $DESTFILE;

my $nonzero = 0;
my $data;
open(F, "< $filename") or die "$!\n";
while(<F>) {
    my @a = split;
    if (scalar(@a)==2) {
        my $t8 = @a[0];
        my $r = @a[1];
        $nonzero++ if $r > 0;
        $data .= "$t8 $r\n" if $nonzero;
    }
}
close(F);

die "Error: no points with r > 0.\n" unless $nonzero;


my $gnuplot =<<EOF;
set fit quiet
set fit prescale
set fit logfile "/dev/null"
a0 = 6.819220e+01
a1 = -8.533710e+01
a2 = -5.529960e+01
a3 = -5.028930e-01
a4 = -1.358690e-01
a5 = -1.193430e-01
a6 = 8.333330e-01

lambda(T9) = exp(a0 + a1/T9 + a2/(T9**(1.0/3.0)) + a3*(T9**(1.0/3.0)) + a4*T9 + a5*(T9**(5.0/3.0)) + a6*log(T9)) 

lambda_log(T9) = a0 + a1/T9 + a2/(T9**(1.0/3.0)) + a3*(T9**(1.0/3.0)) + a4*T9 + a5*(T9**(5.0/3.0)) + a6*log(T9)

fit [1.1:1.3] lambda_log(x) "-" using (\$1):(log(\$2 + 1e-300)) via a0, a1, a2, a3, a4, a5, a6
$data
e
set print "$dir/myreaclib.txt" append
print sprintf("%13.6E%13.6E%13.6E%13.6E%22s", a0, a1, a2, a3, "")
print sprintf("%13.6E%13.6E%13.6E%35s", a4, a5, a6, "")
#set term x11
#set multiplot layout 2,1
#set grid
#set xlabel "T9"
#set ylabel "Deviation, %"
#set xrange [0.5:1.9]
#plot "$filename" skip 1 u (\$1):(abs(0.01 * (\$2 / lambda(\$1) - 1))) w lp t "fit deviation"
#set logscale y
#set ylabel "Rate"
#plot "$filename" skip 1 u 1:2 w p t "TALYS points", lambda(x) w l t "gnuplot fit"
EOF

open(GNUPLOT, "| gnuplot");
print GNUPLOT $gnuplot;
close(GNUPLOT);
